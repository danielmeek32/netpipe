#include "Application.h"

#include <csignal>
#include <vector>

#include "Console.h"
#include "Options.h"
#include "Socket.h"

int Application::run(const std::list<std::string> args)
{
	// read options
	if (args.size() == 0)
	{
		Application::showHelp();
		return 0;
	}

	Options options;
	try
	{
		options = Options(args);
	}
	catch (const UnknownOptionException& exception)
	{
		Console::writeLine("Unknown option \"" + exception.getOption() + "\"");
		Console::writeLine();
		Application::showHelp();
		return 1;
	}
	catch (const MissingConnectionParameterException& exception)
	{
		Console::writeLine("One or more connection parameters are missing");
		Console::writeLine();
		Application::showHelp();
		return 1;
	}
	catch (const InvalidPortException& exception)
	{
		Console::writeLine("Invalid port number \"" + exception.getPort() + "\"");
		Console::writeLine();
		Application::showHelp();
		return 1;
	}

	if (options.help)
	{
		Application::showHelp();
		return 0;
	}

	// set up signal handlers
	std::signal(SIGINT, Application::signalHandler);
	std::signal(SIGTERM, Application::signalHandler);

	// perform sockets startup
	Socket::startup();

	// get the destination addresses of any outgoing sockets
	SocketDestinationAddress* first_address = nullptr;
	SocketDestinationAddress* second_address = nullptr;
	bool resolve_error = false;
	if (!options.firstListening)
	{
		try
		{
			if (options.verbose)
			{
				Console::writeLine("Resolving hostname \"" + options.firstHostname + "\"");
			}
			first_address = new SocketDestinationAddress(Socket::lookup(options.firstHostname, options.firstPort));
		}
		catch (const SocketErrorException& exception)
		{
			if (!Application::interrupted)
			{
				Console::writeLine("Error resolving hostname \"" + options.firstHostname + "\": " + exception.getError());
			}
			resolve_error = true;
		}
	}
	if (!resolve_error)
	{
		if (!options.secondListening)
		{
			try
			{
				if (options.verbose)
				{
					Console::writeLine("Resolving hostname \"" + options.secondHostname + "\"");
				}
				second_address = new SocketDestinationAddress(Socket::lookup(options.secondHostname, options.secondPort));
			}
			catch (const SocketErrorException& exception)
			{
				if (!Application::interrupted)
				{
					Console::writeLine("Error resolving hostname \"" + options.secondHostname + "\": " + exception.getError());
				}
				resolve_error = true;
			}
		}
	}

	if (!resolve_error)
	{
		// run in a loop
		for (;;)
		{
			Application::RunStatus result = Application::runOnce(options.firstListening ? nullptr : first_address, options.firstPort, options.firstLocalOnly, options.secondListening ? nullptr : second_address, options.secondPort, options.secondLocalOnly, options.ordered, options.verbose);

			// decide whether to terminate
			if (result == Application::RunStatus::SUCCESS && !options.indefinite)
			{
				break;
			}
			else if (result == Application::RunStatus::EARLY_DISCONNECT && !options.indefinite && !options.retry)
			{
				break;
			}
			else if (result == Application::RunStatus::ERROR)
			{
				break;
			}
			else if (result == Application::RunStatus::INTERRUPT || Application::interrupted)
			{
				break;
			}

			// show an informational message if running again
			if (options.verbose)
			{
				if (result == Application::RunStatus::SUCCESS)
				{
					Console::writeLine("Pipe was terminated normally, running again");
				}
				else if (result == Application::RunStatus::EARLY_DISCONNECT)
				{
					Console::writeLine("One socket was closed before the other one was connected, running again");
				}
			}
		}
	}

	// show message if interrupted
	if (Application::interrupted)
	{
		Console::writeLine("Interrupted");
	}

	// cleanup
	delete first_address;
	delete second_address;

	// perform sockets shutdown
	Socket::shutdown();

	// remove signal handlers
	std::signal(SIGINT, SIG_DFL);
	std::signal(SIGTERM, SIG_DFL);

	return 0;
}

void Application::showHelp()
{
	Console::writeLine("Usage: netpipe {hostname|-l|-L} port {hostname|-l|-L} port [-o] [-i|-r] [-v]");
	Console::writeLine();
	Console::writeLine("Netpipe fulfils a variety of network proxying applications in a manner similar to Netcat or SSH port forwards, but without the extra overhead or complication. It creates two TCP connections and then establishes a \"pipe\" between them. In particular, it can connect two outgoing connections or two listening sockets together in a way not normally possible with TCP, and as such may be useful for getting around NATs, firewalls, or other situations where a \"backwards\" connection is needed.");
	Console::writeLine();
	Console::writeLine("The first four parameters specify the hostname and port for each end of the pipe. If -l or -L is given instead of a hostname, a listening socket will be created on the specified port, otherwise an outgoing connection will be made to the given hostname or IP address. If -L is given, the listening socket will accept only connections from localhost, otherwise connections from any host are accepted.");
	Console::writeLine();
	Console::writeLine("By default, both ends of the connection are established in parallel. That is to say, outgoing connections are made in whichever order they are accepted, and listening sockets are opened at the same time and connections are accepted in whichever order they arrive. If one end of the pipe is an outgoing connection and the other end is a listening socket, the outgoing connection will be established while the listening socket is waiting for an incoming connection.");
	Console::writeLine();
	Console::writeLine("By specifying the -o option, the connection at the first end of the pipe will be established first. If the first end of the pipe is an outgoing connection, Netpipe will wait until this connection is established. If the first end of the pipe is a listening socket, Netpipe will wait until an incoming connection is received and accepted. Only then will an outgoing connection at the other end be initiated or a listening socket at the other end be opened.");
	Console::writeLine();
	Console::writeLine("Normally, Netpipe will establish one pipe and then terminate as soon as either end disconnects. If one end is disconnected before the other end is connected, Netpipe terminates immediately. This behavior can be changed by passing either the -i option in which case Netpipe will run endlessly until it is interrupted, establishing another pipe whenever the previous pipe is disconnected, or the -r option in which case Netpipe will keep trying to establish a pipe if one end disconnects before the other end is connected but will terminate once a successful pipe has been established and subsequently disconnected. If an error occurs at any time, Netpipe will always terminate immediately regardless of which (if any) of the aforementioned options are in effect. This includes network errors such as host unreachable or connection refused, to avoid inadvertently flooding other systems with network traffic.");
	Console::writeLine();
	Console::writeLine("-i\tRun indefinitely until interrupted, creating a new pipe every time the existing one is disconnected, rather than terminating as soon as the first pipe is disconnected.");
	Console::writeLine("-h\tShow usage information. All other options will be ignored if this option is given.");
	Console::writeLine("-l\tOpen a listening socket on the specified port rather than creating an outgoing connection.");
	Console::writeLine("-L\tSame as -l, except that the listening socket will accept only localhost connections (more secure if remote connections to a listening socket aren't required).");
	Console::writeLine("-o\tEstablish connections in order. The first end of the pipe must be successfully connected before the second end will be set up.");
	Console::writeLine("-r\tRetry if one end of the pipe disconnects before the other end is connected, but terminate once a successfully-established pipe is disconnected. This option is ignored if -i is given.");
	Console::writeLine("-v\tShow informational messages.");
}

Application::RunStatus Application::runOnce(const SocketDestinationAddress* firstAddress, uint16_t firstPort, bool firstLocalOnly, const SocketDestinationAddress* secondAddress, uint16_t secondPort, bool secondLocalOnly, bool ordered, bool verbose)
{
	Application::RunStatus result = Application::RunStatus::SUCCESS;

	Socket* socket_a = nullptr;
	Socket* socket_b = nullptr;
	Socket* socket_a_connecting = nullptr;	// used while establishing the connections to distinguish a listening or pending outgoing socket from a connected socket
	Socket* socket_b_connecting = nullptr;	// used while establishing the connections to distinguish a listening or pending outgoing socket from a connected socket

	try
	{
		bool first_listening = firstAddress == nullptr;
		bool second_listening = secondAddress == nullptr;

		// open sockets
		if (ordered)
		{
			// create the first socket
			try
			{
				if (first_listening)
				{
					socket_a_connecting = new Socket(firstPort, firstLocalOnly);
				}
				else
				{
					socket_a_connecting = new Socket(*firstAddress);
				}
			}
			catch (const SocketErrorException& exception)
			{
				Console::writeLine("Error creating first socket: " + exception.getError());
				throw exception;
			}
			if (verbose)
			{
				Console::writeLine("Created first socket");
			}

			// wait for connection to be established on the first socket
			do
			{
				// wait for socket to be ready
				std::vector<Socket> select_read;
				std::vector<Socket> select_write;
				if (first_listening)
				{
					select_read.push_back(*socket_a_connecting);
				}
				else
				{
					select_write.push_back(*socket_a_connecting);
				}
				bool* select_result;
				try
				{
					select_result = Socket::select(select_read.data(), select_read.size(), select_write.data(), select_write.size());
				}
				catch (const SocketErrorException& exception)
				{
					Console::writeLine("Error connecting sockets: " + exception.getError());
					throw exception;
				}

				// handle socket
				if (select_result[0])
				{
					if (first_listening)
					{
						// accept connection on listening socket
						try
						{
							socket_a = new Socket(socket_a_connecting->accept());
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error accepting connection on first socket: " + exception.getError());
							throw exception;
						}
						socket_a_connecting->close();
						delete socket_a_connecting;
						socket_a_connecting = nullptr;
					}
					else
					{
						// check if outgoing socket is connected and store it if it is
						try
						{
							if (socket_a_connecting->isConnectionEstablished())	// always returns true, or throws exception
							{
								socket_a = socket_a_connecting;
								socket_a_connecting = nullptr;
							}
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error establishing connection on first socket: " + exception.getError());
							throw exception;
						}
					}
				}

				delete select_result;

				// terminate if interrupted
				if (Application::interrupted)
				{
					throw ApplicationRunInterruptedException();
				}
			}
			while (socket_a == nullptr);
			if (socket_a != nullptr && verbose)
			{
				Console::writeLine("First socket is connected");
			}

			// create the second socket
			try
			{
				if (second_listening)
				{
					socket_b_connecting = new Socket(secondPort, secondLocalOnly);
				}
				else
				{
					socket_b_connecting = new Socket(*secondAddress);
				}
			}
			catch (const SocketErrorException& exception)
			{
				Console::writeLine("Error creating second socket: " + exception.getError());
				throw exception;
			}
			if (verbose)
			{
				Console::writeLine("Created second socket");
			}

			// wait for connection to be established on the second socket, while monitoring the first socket for closure
			bool a_has_data = false;	// used to keep track of whether or not A has already been tested after select returns it as readable (avoids constantly re-testing A for closure)
			do
			{
				// wait for socket to be ready
				std::vector<Socket> select_read;
				std::vector<Socket> select_write;
				if (!a_has_data)
				{
					select_read.push_back(*socket_a);
				}
				if (second_listening)
				{
					select_read.push_back(*socket_b_connecting);
				}
				else
				{
					select_write.push_back(*socket_b_connecting);
				}
				bool* select_result;
				try
				{
					select_result = Socket::select(select_read.data(), select_read.size(), select_write.data(), select_write.size());
				}
				catch (const SocketErrorException& exception)
				{
					Console::writeLine("Error connecting sockets: " + exception.getError());
					throw exception;
				}

				// handle A
				if (!a_has_data)
				{
					if (select_result[0])	// accesses the first of the read sockets (which will always correspond to A if present)
					{
						// check if connected socket has been closed
						try
						{
							if (socket_a->isClosedByRemoteEnd())
							{
								Console::writeLine("First socket was closed before the second socket was connected");
								break;
							}
							else
							{
								a_has_data = true;
							}
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error connecting sockets: " + exception.getError());
							throw exception;
						}
					}
				}

				// handle B
				if (second_listening)
				{
					if (select_result[select_read.size() - 1])	// accesses the last of the read sockets, no matter how many read sockets there are (which will always correspond to B if present)
					{
						// accept connection on listening socket
						try
						{
							socket_b = new Socket(socket_b_connecting->accept());
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error accepting connection on second socket: " + exception.getError());
							throw exception;
						}
						socket_b_connecting->close();
						delete socket_b_connecting;
						socket_b_connecting = nullptr;
					}
				}
				else
				{
					if (select_result[select_read.size() + select_write.size() - 1])	// accesses the last of the write sockets, no matter how many read or write sockets there are (which will always correspond to B if present)
					{
						// check if outgoing socket is connected and store it if it is
						try
						{
							if (socket_b_connecting->isConnectionEstablished())	// always returns true, or throws exception
							{
								socket_b = socket_b_connecting;
								socket_b_connecting = nullptr;
							}
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error establishing connection on second socket: " + exception.getError());
							throw exception;
						}
					}
				}

				delete select_result;

				// terminate if interrupted
				if (Application::interrupted)
				{
					throw ApplicationRunInterruptedException();
				}
			}
			while (socket_b == nullptr);
			if (socket_b != nullptr && verbose)
			{
				Console::writeLine("Second socket is connected");
			}
		}
		else
		{
			// create the first socket
			try
			{
				if (first_listening)
				{
					socket_a_connecting = new Socket(firstPort, firstLocalOnly);
				}
				else
				{
					socket_a_connecting = new Socket(*firstAddress);
				}
			}
			catch (const SocketErrorException& exception)
			{
				Console::writeLine("Error creating first socket: " + exception.getError());
				throw exception;
			}
			if (verbose)
			{
				Console::writeLine("Created first socket");
			}

			// create the second socket
			try
			{
				if (second_listening)
				{
					socket_b_connecting = new Socket(secondPort, secondLocalOnly);
				}
				else
				{
					socket_b_connecting = new Socket(*secondAddress);
				}
			}
			catch (const SocketErrorException& exception)
			{
				Console::writeLine("Error creating second socket: " + exception.getError());
				throw exception;
			}
			if (verbose)
			{
				Console::writeLine("Created second socket");
			}

			// wait for both connections to be established
			bool a_has_data = false;	// used once A is connected but before B is connected, to keep track of whether or not A has already been tested after select returns it as readable (avoids constantly re-testing A for closure)
			bool b_has_data = false;	// used once B is connected but before A is connected, to keep track of whether or not B has already been tested after select returns it as readable (avoids constantly re-testing B for closure)
			do
			{
				// wait for socket to be ready
				std::vector<Socket> select_read;
				std::vector<Socket> select_write;
				if (socket_a_connecting != nullptr)
				{
					if (first_listening)
					{
						select_read.push_back(*socket_a_connecting);
					}
					else
					{
						select_write.push_back(*socket_a_connecting);
					}
				}
				else if (socket_a != nullptr)
				{
					if (!a_has_data)
					{
						select_read.push_back(*socket_a);
					}
				}
				if (socket_b_connecting != nullptr)
				{
					if (second_listening)
					{
						select_read.push_back(*socket_b_connecting);
					}
					else
					{
						select_write.push_back(*socket_b_connecting);
					}
				}
				else if (socket_b != nullptr)
				{
					if (!b_has_data)
					{
						select_read.push_back(*socket_b);
					}
				}
				bool* select_result;
				try
				{
					select_result = Socket::select(select_read.data(), select_read.size(), select_write.data(), select_write.size());
				}
				catch (const SocketErrorException& exception)
				{
					Console::writeLine("Error connecting sockets: " + exception.getError());
					throw exception;
				}

				// handle A
				if (socket_a_connecting != nullptr)
				{
					if (first_listening)
					{
						if (select_result[0])	// accesses the first of the read sockets (which will always correspond to A if present)
						{
							// accept connection on listening socket
							try
							{
								socket_a = new Socket(socket_a_connecting->accept());
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error accepting connection on first socket: " + exception.getError());
								throw exception;
							}
							socket_a_connecting->close();
							delete socket_a_connecting;
							socket_a_connecting = nullptr;
						}
					}
					else
					{
						if (select_result[select_read.size()])	// accesses the first of the write sockets, no matter how many read sockets there are (which will always correspond to A if present)
						{
							// check if outgoing socket is connected and store it if it is
							try
							{
								if (socket_a_connecting->isConnectionEstablished())	// always returns true, or throws exception
								{
									socket_a = socket_a_connecting;
									socket_a_connecting = nullptr;
								}
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error establishing connection on first socket: " + exception.getError());
								throw exception;
							}
						}
					}
					if (socket_a != nullptr && verbose)
					{
						Console::writeLine("First socket is connected");
					}
				}
				else if (socket_a != nullptr)
				{
					if (!a_has_data)
					{
						if (select_result[0])	// accesses the first of the read sockets (which will always correspond to A if present)
						{
							// check if connected socket has been closed
							try
							{
								if (socket_a->isClosedByRemoteEnd())
								{
									Console::writeLine("First socket was closed before the second socket was connected");
									break;
								}
								else
								{
									a_has_data = true;
								}
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error connecting sockets: " + exception.getError());
								throw exception;
							}
						}
					}
				}

				// handle B
				if (socket_b_connecting != nullptr)
				{
					if (second_listening)
					{
						if (select_result[select_read.size() - 1])	// accesses the last of the read sockets, no matter how many read sockets there are (which will always correspond to B if present)
						{
							// accept connection on listening socket
							try
							{
								socket_b = new Socket(socket_b_connecting->accept());
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error accepting connection on second socket: " + exception.getError());
								throw exception;
							}
							socket_b_connecting->close();
							delete socket_b_connecting;
							socket_b_connecting = nullptr;
						}
					}
					else
					{
						if (select_result[select_read.size() + select_write.size() - 1])	// accesses the last of the write sockets, no matter how many read or write sockets there are (which will always correspond to B if present)
						{
							// check if outgoing socket is connected and store it if it is
							try
							{
								if (socket_b_connecting->isConnectionEstablished())	// always returns true, or throws exception
								{
									socket_b = socket_b_connecting;
									socket_b_connecting = nullptr;
								}
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error establishing connection on second socket: " + exception.getError());
								throw exception;
							}
						}
					}
					if (socket_b != nullptr && verbose)
					{
						Console::writeLine("Second socket is connected");
					}
				}
				else if (socket_b != nullptr)
				{
					if (!b_has_data)
					{
						if (select_result[select_read.size() - 1])	// accesses the last of the read sockets, no matter how many read sockets there are (which will always correspond to B if present)
						{
							// check if connected socket has been closed
							try
							{
								if (socket_b->isClosedByRemoteEnd())
								{
									Console::writeLine("Second socket was closed before the first socket was connected");
									break;
								}
								else
								{
									b_has_data = true;
								}
							}
							catch (const SocketErrorException& exception)
							{
								Console::writeLine("Error connecting sockets: " + exception.getError());
								throw exception;
							}
						}
					}
				}

				delete select_result;

				// terminate if interrupted
				if (Application::interrupted)
				{
					throw ApplicationRunInterruptedException();
				}
			}
			while (socket_a == nullptr || socket_b == nullptr);
		}

		if (Application::interrupted)	// check for pending interrupt before continuing
		{
			throw ApplicationRunInterruptedException();
		}
		else if (socket_a == nullptr || socket_b == nullptr)	// this will happen if one of the sockets disconnects before the other one has connected
		{
			result = Application::RunStatus::EARLY_DISCONNECT;
		}
		else
		{
			// run the pipe
			for (;;)
			{
				bool* select_result;
				try
				{
					Socket sockets[] = { *socket_a, *socket_b };
					select_result = Socket::select(sockets, 2, nullptr, 0);
				}
				catch (const SocketErrorException& exception)
				{
					Console::writeLine("Error waiting for data: " + exception.getError());
					throw exception;
				}
				bool select_a = select_result[0];
				bool select_b = select_result[1];
				delete select_result;

				if (select_a)	// socket A
				{
					// read data from the socket
					uint8_t buffer[1024];
					uint32_t count;
					try
					{
						count = socket_a->read(buffer, 1024);
					}
					catch (const SocketErrorException& exception)
					{
						Console::writeLine("Error reading data from first socket: " + exception.getError());
						throw exception;
					}
					if (count == 0)
					{
						// the socket has been closed
						if (verbose)
						{
							Console::writeLine("First socket was closed by the remote end");
						}
						break;
					}

					// write the data to the other socket
					uint32_t remaining = count;
					while (remaining > 0)
					{
						uint32_t sent;
						try
						{
							sent = socket_b->write(buffer + (count - remaining), remaining);
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error writing data to second socket: " + exception.getError());
							throw exception;
						}
						if (sent == 0)
						{
							// the socket has been closed
							break;	// (this only terminates the loop for writing data to the socket, see conditional statement after the end of the loop for actual termination of the pipe loop)
						}
						remaining -= sent;
					}
					if (remaining > 0)
					{
						// the socket has been closed
						if (verbose)
						{
							Console::writeLine("Second socket was closed by the remote end");
						}
						break;
					}
				}

				if (select_b)	// socket B
				{
					// read data from the socket
					uint8_t buffer[1024];
					uint32_t count;
					try
					{
						count = socket_b->read(buffer, 1024);
					}
					catch (const SocketErrorException& exception)
					{
						Console::writeLine("Error reading data from second socket: " + exception.getError());
						throw exception;
					}
					if (count == 0)
					{
						// the socket has been closed
						if (verbose)
						{
							Console::writeLine("Second socket was closed by the remote end");
						}
						break;
					}

					// write the data to the other socket
					uint32_t remaining = count;
					while (remaining > 0)
					{
						uint32_t sent;
						try
						{
							sent = socket_a->write(buffer + (count - remaining), remaining);
						}
						catch (const SocketErrorException& exception)
						{
							Console::writeLine("Error writing data to first socket: " + exception.getError());
							throw exception;
						}
						if (sent == 0)
						{
							// the socket has been closed
							break;	// (this only terminates the loop for writing data to the socket, see conditional statement after the end of the loop for actual termination of the pipe loop)
						}
						remaining -= sent;
					}
					if (remaining > 0)
					{
						// the socket has been closed
						if (verbose)
						{
							Console::writeLine("First socket was closed by the remote end");
						}
						break;
					}
				}

				// terminate if interrupted
				if (Application::interrupted)
				{
					throw ApplicationRunInterruptedException();
				}
			}
		}
	}
	catch (const SocketErrorException& exception)
	{
		// this closes everything and sets the result status regardless of where the exception occurred (the original exception may be rethrown if it was caught, or ignored)
		result = Application::RunStatus::ERROR;
	}
	catch (const SocketInterruptedException& exception)
	{
		// this closes everything and sets the result status regardless of where the exception occurred (the original exception may be rethrown if it was caught, or ignored)
		result = Application::RunStatus::INTERRUPT;
	}
	catch (const ApplicationRunInterruptedException& exception)
	{
		// this closes everything and sets the result status regardless of where the exception occurred (this allows easy termination of the main body with proper cleanup regardless of where the interrupt occurred)
		result = Application::RunStatus::INTERRUPT;
	}

	// close any sockets that are still open (in case of error, successful termination, etc. this will make sure that everything is properly closed regardless of the state at the time of the termination)
	if (socket_a != nullptr)
	{
		socket_a->close();
		delete socket_a;
	}
	else if (socket_a_connecting != nullptr)
	{
		socket_a_connecting->close();
		delete socket_a_connecting;
	}
	if (socket_b != nullptr)
	{
		socket_b->close();
		delete socket_b;
	}
	else if (socket_b_connecting != nullptr)
	{
		socket_b_connecting->close();
		delete socket_b_connecting;
	}

	return result;
}

void Application::signalHandler(int signal)
{
	Application::interrupted = true;
}

std::atomic_bool Application::interrupted(false);
