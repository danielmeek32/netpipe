#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <atomic>
#include <list>
#include <string>

#include "Socket.h"

class Application
{
	public:
		static int run(const std::list<std::string> args);

	private:
		#ifdef _WIN32
			#undef ERROR	// conflicts with RunStatus
		#endif
		enum RunStatus
		{
			SUCCESS,
			EARLY_DISCONNECT,
			ERROR,
			INTERRUPT,
		};

		static void showHelp();
		static RunStatus runOnce(const SocketDestinationAddress* firstAddress, uint16_t firstPort, bool firstLocalOnly, const SocketDestinationAddress* secondAddress, uint16_t secondPort, bool secondLocalOnly, bool ordered, bool verbose);

		static void signalHandler(int signal);

		static std::atomic_bool interrupted;
};

class ApplicationRunInterruptedException: public std::exception
{
	// empty
};

#endif
