#include "Console.h"

#include <iostream>

void Console::writeLine(std::string message)
{
	std::cout << message << std::endl;
}

void Console::writeLine()
{
	std::cout << std::endl;
}
