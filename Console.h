#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <string>

class Console
{
	public:
		static void writeLine(std::string message);
		static void writeLine();
};

#endif
