#include "Options.h"

#include <stdexcept>

Options::Options(const std::list<std::string> args)
{
	bool expecting_port = false;
	bool have_first_connection = false;
	bool have_second_connection = false;
	for (std::list<std::string>::const_iterator iterator = args.begin(); iterator != args.end(); iterator++)
	{
		std::string arg = *iterator;
		if (arg != "")
		{
			if (!expecting_port)
			{
				// interpret the argument normally
				if (arg.length() == 2 && arg[0] == '-')
				{
					switch (arg[1])
					{
						case 'h':
							this->help = true;
							break;
						case 'v':
							this->verbose = true;
							break;
						case 'i':
							this->indefinite = true;
							break;
						case 'o':
							this->ordered = true;
							break;
						case 'r':
							this->retry = true;
							break;
						case 'l':
						case 'L':
							expecting_port = true;
							break;
						default:
							throw UnknownOptionException(arg);
					}
				}
				else
				{
					expecting_port = true;
				}

				if (expecting_port)
				{
					// this means we've got either a hostname or a listening socket option
					std::string hostname = "";
					bool listening = false;
					bool local_only = false;
					if (arg == "-l")
					{
						listening = true;
					}
					else if (arg == "-L")
					{
						listening = true;
						local_only = true;
					}
					else
					{
						hostname = arg;
					}

					if (!have_first_connection)
					{
						this->firstHostname = hostname;
						this->firstListening = listening;
						this->firstLocalOnly = local_only;
					}
					else if (!have_second_connection)
					{
						this->secondHostname = hostname;
						this->secondListening = listening;
						this->secondLocalOnly = local_only;
					}
					else
					{
						throw UnknownOptionException(arg);
					}
				}
			}
			else
			{
				// try to interpret the argument as a port number
				int port_int = 0;
				if (arg.find_first_not_of("0123456789") != std::string::npos)
				{
					throw InvalidPortException(arg);
				}
				else
				{
					try
					{
						port_int = std::stoi(arg, nullptr, 10);
					}
					catch (const std::invalid_argument& exception)
					{
						throw InvalidPortException(arg);
					}
					catch (const std::out_of_range& exception)
					{
						throw InvalidPortException(arg);
					}
				}
				if (port_int < 1 || port_int > 65535)
				{
					throw InvalidPortException(arg);
				}
				uint16_t port = (uint16_t) port_int;

				if (!have_first_connection)
				{
					this->firstPort = port;
					have_first_connection = true;
					expecting_port = false;
				}
				else if (!have_second_connection)
				{
					this->secondPort = port;
					have_second_connection = true;
					expecting_port = false;
				}
				else
				{
					throw UnknownOptionException(arg);
				}
			}
		}
	}

	if (!this->help)
	{
		if (!(have_first_connection && have_second_connection))
		{
			throw MissingConnectionParameterException();
		}
	}
}

UnknownOptionException::UnknownOptionException(const std::string option)
{
	this->option = option;
}

std::string UnknownOptionException::getOption() const
{
	return this->option;
}

InvalidPortException::InvalidPortException(const std::string port)
{
	this->port = port;
}

std::string InvalidPortException::getPort() const
{
	return this->port;
}
