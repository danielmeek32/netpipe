#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <exception>
#include <list>
#include <stdint.h>
#include <string>

class Options
{
	public:
		Options() {}	// FIXME: this is messy but Application requires it because of exception handling
		Options(const std::list<std::string> args);

		bool help = false;
		bool verbose = false;

		std::string firstHostname = "";
		bool firstListening = false;
		bool firstLocalOnly = false;
		uint16_t firstPort = 0;
		std::string secondHostname = "";
		bool secondListening = false;
		bool secondLocalOnly = false;
		uint16_t secondPort = 0;

		bool indefinite = false;
		bool ordered = false;
		bool retry = false;
};

class UnknownOptionException: public std::exception
{
	public:
		UnknownOptionException(const std::string option);

		std::string getOption() const;

	private:
		std::string option;
};

class MissingConnectionParameterException: public std::exception
{
	// empty
};

class InvalidPortException: public std::exception
{
	public:
		InvalidPortException(const std::string port);

		std::string getPort() const;

	private:
		std::string port;
};

#endif
