# Netpipe

Version: 1.0.1

## License

Copyright (C) 2019 danielmeek32

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Description

Netpipe fulfils a variety of network proxying applications in a manner similar to Netcat or SSH port forwards, but without the extra overhead or complication. It creates two TCP connections and then establishes a "pipe" between them. In particular, it can connect two outgoing connections or two listening sockets together in a way not normally possible with TCP, and as such may be useful for getting around NATs, firewalls, or other situations where a "backwards" connection is needed.

## Usage

`netpipe {hostname|-l|-L} port {hostname|-l|-L} port [-o] [-i|-r] [-v]`

The first four parameters specify the hostname and port for each end of the pipe. If `-l` or `-L` is given instead of a hostname, a listening socket will be created on the specified port, otherwise an outgoing connection will be made to the given hostname or IP address. If `-L` is given, the listening socket will accept only connections from localhost, otherwise connections from any host are accepted.

By default, both ends of the connection are established in parallel. That is to say, outgoing connections are made in whichever order they are accepted, and listening sockets are opened at the same time and connections are accepted in whichever order they arrive. If one end of the pipe is an outgoing connection and the other end is a listening socket, the outgoing connection will be established while the listening socket is waiting for an incoming connection.

By specifying the `-o` option, the connection at the first end of the pipe will be established first. If the first end of the pipe is an outgoing connection, Netpipe will wait until this connection is established. If the first end of the pipe is a listening socket, Netpipe will wait until an incoming connection is received and accepted. Only then will an outgoing connection at the other end be initiated or a listening socket at the other end be opened.

Normally, Netpipe will establish one pipe and then terminate as soon as either end disconnects. If one end is disconnected before the other end is connected, Netpipe terminates immediately. This behavior can be changed by passing either the `-i` option in which case Netpipe will run endlessly until it is interrupted, establishing another pipe whenever the previous pipe is disconnected, or the `-r` option in which case Netpipe will keep trying to establish a pipe if one end disconnects before the other end is connected but will terminate once a successful pipe has been established and subsequently disconnected. If an error occurs at any time, Netpipe will always terminate immediately regardless of which (if any) of the aforementioned options are in effect. This includes network errors such as host unreachable or connection refused, to avoid inadvertently flooding other systems with network traffic.

## Options

`-i`	Run indefinitely until interrupted, creating a new pipe every time the existing one is disconnected, rather than terminating as soon as the first pipe is disconnected.  
`-h`	Show usage information. All other options will be ignored if this option is given.  
`-l`	Open a listening socket on the specified port rather than creating an outgoing connection.  
`-L`	Same as `-l`, except that the listening socket will accept only localhost connections (more secure if remote connections to a listening socket aren't required).  
`-o`	Establish connections in order. The first end of the pipe must be successfully connected before the second end will be set up.  
`-r`	Retry if one end of the pipe disconnects before the other end is connected, but terminate once a successfully-established pipe is disconnected. This option is ignored if `-i` is given.  
`-v`	Show informational messages.

## Building

Netpipe does not have any dependencies. CMake build files are included for convenience, although Netpipe can also be compiled without CMake.

To compile Netpipe using CMake:

    mkdir build
    cd build
    cmake ..
    make

To compile Netpipe using only the GCC compiler:

    g++ netpipe.cpp Application.cpp Console.cpp Options.cpp Socket.cpp -o netpipe

In the latter case, additional compiler flags may be given if desired.

Netpipe should compile with MinGW, although Windows does not properly support interrupts so pressing `ctrl+c` to terminate Netpipe won't always work correctly. If compiling by hand, you should probably add `-static ws2_32 -static-libgcc -static-libstdc++` to the compiler flags otherwise Netpipe will depend on other DLL files being available at runtime (if you use CMake these flags will be added automatically).
