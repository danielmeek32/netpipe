#include "Socket.h"

#include <cstring>
#include <new>

#ifdef _WIN32
	#include <windows.h>
	#include <ws2tcpip.h>
#else
	#include <errno.h>
	#include <arpa/inet.h>
	#include <netinet/in.h>
	#include <sys/ioctl.h>

	#define INVALID_SOCKET (-1)
	#define SOCKET_ERROR (-1)
#endif

void Socket::startup()
{
	#ifdef _WIN32
		static WSADATA wsa_data;
		WSAStartup(MAKEWORD(2, 2), &wsa_data);
	#endif
}

void Socket::shutdown()
{
	#ifdef _WIN32
		WSACleanup();
	#endif
}

bool* Socket::select(const Socket readSockets[], int readCount, const Socket writeSockets[], int writeCount)
{
	int nfds = 0;
	fd_set readfds;
	fd_set writefds;
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	for (int index = 0; index < readCount; index++)
	{
		Socket socket = readSockets[index];
		#ifndef _WIN32
			if (socket.socket + 1 > nfds)
			{
				nfds = socket.socket + 1;
			}
		#endif
		FD_SET(socket.socket, &readfds);
	}
	for (int index = 0; index < writeCount; index++)
	{
		Socket socket = writeSockets[index];
		#ifndef _WIN32
			if (socket.socket + 1 > nfds)
			{
				nfds = socket.socket + 1;
			}
		#endif
		FD_SET(socket.socket, &writefds);
	}

	// FIXME: interrupt during select is not supported on Windows
	if (::select(nfds, &readfds, &writefds, NULL, NULL) == SOCKET_ERROR)
	{
		Socket::throwSocketException();
	}

	bool* result = new bool[readCount + writeCount];
	for (int index = 0; index < readCount; index++)
	{
		if (FD_ISSET(readSockets[index].socket, &readfds))
		{
			result[index] = true;
		}
		else
		{
			result[index] = false;
		}
	}
	for (int index = 0; index < writeCount; index++)
	{
		if (FD_ISSET(writeSockets[index].socket, &writefds))
		{
			result[index + readCount] = true;
		}
		else
		{
			result[index + readCount] = false;
		}
	}

	return result;
}

SocketDestinationAddress Socket::lookup(const std::string hostname, uint16_t port)
{
	return SocketDestinationAddress(hostname, port);
}

Socket::Socket(const SocketDestinationAddress& address): listening(false)
{
	// create the socket
	this->socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->socket == INVALID_SOCKET)
	{
		Socket::throwSocketException();
	}

	// connect the socket
	#ifdef _WIN32
		long unsigned int value = 1;
		ioctlsocket(this->socket, FIONBIO, &value);
	#else
		ioctl(this->socket, FIONBIO, &((const long unsigned int&) (long unsigned int){ 1 }));
	#endif
	if (::connect(this->socket, address.address, address.address_length) == SOCKET_ERROR)
	{
		#ifdef _WIN32
			int error = ::WSAGetLastError();
			if (error != WSAEINPROGRESS && error != WSAEWOULDBLOCK)
		#else
			int error = errno;
			if (error != EINPROGRESS)
		#endif
		{
			Socket::closeSocket(this->socket);
			Socket::throwSocketException(error);
		}
	}
	#ifdef _WIN32
		value = 0;
		ioctlsocket(this->socket, FIONBIO, &value);
	#else
		ioctl(this->socket, FIONBIO, &((const long unsigned int&) (long unsigned int){ 0 }));
	#endif
}

Socket::Socket(uint16_t port, bool localOnly): listening(true)
{
	// create the socket
	this->socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->socket == INVALID_SOCKET)
	{
		Socket::throwSocketException();
	}

	// bind the socket
	::setsockopt(this->socket, SOL_SOCKET, SO_REUSEADDR, (char*) &((const int&) (int){ 1 }), sizeof(int));

	struct sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = localOnly ? ::inet_addr("127.0.0.1") : INADDR_ANY;
	address.sin_port = ::htons(port);
	if (::bind(this->socket, (struct sockaddr*) &address, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
	{
		Socket::closeSocket(this->socket);
		Socket::throwSocketException();
	}

	// listen on the socket
	if (::listen(this->socket, 0) == SOCKET_ERROR)
	{
		Socket::closeSocket(this->socket);
		Socket::throwSocketException();
	}
}

void Socket::close()
{
	Socket::closeSocket(this->socket);
}

bool Socket::isConnectionEstablished()
{
	int status;
	#ifdef _WIN32
		int buffer_length = sizeof(int);
	#else
		socklen_t buffer_length = sizeof(int);
	#endif
	if (getsockopt(this->socket, SOL_SOCKET, SO_ERROR, (char*) &status, &buffer_length) == SOCKET_ERROR)
	{
		Socket::throwSocketException();
	}
	if (status == 0)
	{
		return true;
	}
	else
	{
		Socket::throwSocketException(status);
	}
}

uint32_t Socket::read(uint8_t* buffer, uint32_t length)
{
	if (this->listening)
	{
		throw SocketInvalidOperationException();
	}

	ssize_t count = recv(this->socket, (char*) buffer, length, 0);
	if (count >= 0)
	{
		return (uint32_t) count;
	}
	else
	{
		Socket::throwSocketException();
	}
}

uint32_t Socket::write(const uint8_t* buffer, uint32_t length)
{
	if (this->listening)
	{
		throw SocketInvalidOperationException();
	}

	ssize_t count = send(this->socket, (char*) buffer, length, 0);
	if (count >= 0)
	{
		return (uint32_t) count;
	}
	else
	{
		Socket::throwSocketException();
	}
}

bool Socket::isClosedByRemoteEnd()
{
	if (this->listening)
	{
		throw SocketInvalidOperationException();
	}

	char buffer;
	ssize_t recv_result = recv(this->socket, &buffer, 1, MSG_PEEK);
	if (recv_result > 0)
	{
		return false;
	}
	else if (recv_result == 0)
	{
		return true;
	}
	else
	{
		Socket::throwSocketException();
	}
}

Socket Socket::accept()
{
	if (!this->listening)
	{
		throw SocketInvalidOperationException();
	}

	Socket::socket_t socket = ::accept(this->socket, NULL, NULL);
	if (socket == INVALID_SOCKET)
	{
		Socket::throwSocketException();
	}
	return Socket(socket);
}

void Socket::closeSocket(Socket::socket_t socket)
{
	#ifdef _WIN32
		::closesocket(socket);
	#else
		::close(socket);
	#endif
}

void Socket::throwSocketException()
{
	#ifdef _WIN32
		Socket::throwSocketException(::WSAGetLastError());
	#else
		Socket::throwSocketException(errno);
	#endif
}

void Socket::throwSocketException(int error)
{
	#ifdef _WIN32
		if (error == WSAEINTR)
		{
			throw SocketInterruptedException();
		}
		else
		{
			char* buffer = nullptr;
			if (::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, error, 0, (char*) &buffer, 0, NULL) > 0)
			{
				std::string message(buffer);
				LocalFree(buffer);
				throw SocketErrorException(message);
			}
			else
			{
				throw SocketErrorException();
			}
		}
	#else
		if (error == EINTR)
		{
			throw SocketInterruptedException();
		}
		else
		{
			throw SocketErrorException(std::string(std::strerror(error)));
		}
	#endif
}

Socket::Socket(Socket::socket_t socket): socket(socket), listening(false)
{
	if (this->socket == INVALID_SOCKET)
	{
		throw SocketErrorException();
	}
}

SocketDestinationAddress::SocketDestinationAddress(const SocketDestinationAddress& address)
{
	this->address = (sockaddr*) operator new(address.address_length);
	std::memcpy(this->address, address.address, address.address_length);
	this->address_length = address.address_length;
}

SocketDestinationAddress::~SocketDestinationAddress()
{
	operator delete(this->address);
}

SocketDestinationAddress& SocketDestinationAddress::operator=(const SocketDestinationAddress& address)
{
	operator delete(this->address);

	this->address = (sockaddr*) operator new(address.address_length);
	std::memcpy(this->address, address.address, address.address_length);
	this->address_length = address.address_length;

	return *this;
}

SocketDestinationAddress::SocketDestinationAddress(const std::string hostname, uint16_t port)
{
	struct addrinfo hints;
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_addrlen = 0;
	hints.ai_addr = NULL;
	hints.ai_canonname = NULL;
	hints.ai_next = NULL;
	struct addrinfo* address;

	int getaddrinfo_result = ::getaddrinfo(hostname.c_str(), std::to_string(port).c_str(), &hints, &address);
	if (getaddrinfo_result != 0)
	{
		#ifdef _WIN32
			Socket::throwSocketException();
		#else
			throw SocketErrorException(std::string(gai_strerror(getaddrinfo_result)));
		#endif
	}

	this->address = (sockaddr*) operator new(address->ai_addrlen);
	std::memcpy(this->address, address->ai_addr, address->ai_addrlen);
	this->address_length = address->ai_addrlen;

	::freeaddrinfo(address);
}

SocketErrorException::SocketErrorException(const std::string error)
{
	this->error = error;
}

std::string SocketErrorException::getError() const
{
	return this->error;
}
