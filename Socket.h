#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <exception>
#include <list>
#include <stdint.h>
#include <string>

#ifdef _WIN32
	#include <winsock2.h>
#else
	#include <netdb.h>
	#include <unistd.h>
	#include <sys/socket.h>
	#include <sys/types.h>
#endif

class SocketDestinationAddress;

class Socket
{
	friend class SocketDestinationAddress;

	public:
		static void startup();	// performs Windows socket startup for entire application
		static void shutdown();	// performs Windows socket shutdown for entire application

		// input is an array of sockets to check for readability and an array of sockets to check for writeability
		// either array may be empty or NULL, and the same socket may appear in both arrays
		// output is an array of boolean values indicating whether each socket has the requested state (array is allocated on heap and must be deleted after use)
		// the elements of the output array correspond to the elements of the first input array concatenated with the elements of the second input array
		// (if a socket appears in both input arrays it will have two values in the output array)
		// so for example if sockets A and C are readable and sockets C and D are writeable, the input arrays [ A, B, C, D ] and [ A, C, D ] will give the output [ true, false, true, false, false, true, true ]
		static bool* select(const Socket readSockets[], int readCount, const Socket writeSockets[], int writeCount);

		static SocketDestinationAddress lookup(const std::string hostname, uint16_t port);

		Socket(const SocketDestinationAddress& address);	// creates outgoing socket, select returns socket as writable after connection is established
		Socket(uint16_t port, bool localOnly = false);	// creates listening socket, select returns socket as readable after an incoming connection is received

		void close();

		bool isConnectionEstablished();	// to be used after select returns an outgoing socket as writeable for the first time, to determine the reason (connected vs failed to connect) - always returns true, or throws exception
		uint32_t read(uint8_t* buffer, uint32_t length);
		uint32_t write(const uint8_t* buffer, uint32_t length);
		bool isClosedByRemoteEnd();	// to be used after select returns a connected socket as readable, to determine the reason (data available vs closed)

		Socket accept();

	private:
		#ifdef _WIN32
			typedef SOCKET socket_t;
		#else
			typedef int socket_t;
		#endif

		static void closeSocket(Socket::socket_t socket);
		[[noreturn]] static void throwSocketException();	// same as Socket::throwSocketException(int error) but automatically uses the most recent socket error
		[[noreturn]] static void throwSocketException(int error);	// throws SocketErrorException (with an error message if available) or SocketInterruptedException as appropriate

		Socket(Socket::socket_t socket);	// used to create sockets for incoming connections on a listening socket

		#ifdef _WIN32
			Socket::socket_t socket = INVALID_SOCKET;
		#else
			Socket::socket_t socket = -1;
		#endif
		bool listening;
};

class SocketDestinationAddress
{
	friend class Socket;

	public:
		SocketDestinationAddress(const SocketDestinationAddress& address);	// copy constructor
		~SocketDestinationAddress();

		SocketDestinationAddress& operator=(const SocketDestinationAddress& address);

	private:
		SocketDestinationAddress(const std::string hostname, uint16_t port);

		sockaddr* address;
		#ifdef _WIN32
			size_t address_length;
		#else
			socklen_t address_length;
		#endif
};

class SocketErrorException: public std::exception
{
	public:
		SocketErrorException(): SocketErrorException("Unknown error") {}
		SocketErrorException(const std::string error);

		std::string getError() const;

	private:
		std::string error;
};

class SocketInterruptedException: public std::exception
{
	// empty
};

class SocketInvalidOperationException: public std::exception
{
	// empty
};

#endif
