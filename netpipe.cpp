#include <list>
#include <string>

#include "Application.h"

int main(int argc, char *argv[])
{
	std::list<std::string> args;
	for (int index = 1; index < argc; index++)
	{
		args.push_back(std::string(argv[index]));
	}
	return Application::run(args);
}
